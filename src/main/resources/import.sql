INSERT INTO person (person_id, full_name, years_of_experience, educational_qualification) VALUES (1, 'Robert', 8, 'Engineer');

INSERT INTO person (person_id, full_name, years_of_experience, educational_qualification) VALUES (2, 'John', 8, 'Engineer');

INSERT INTO person (person_id, full_name, years_of_experience, educational_qualification) VALUES (3, 'Peter Johnson', 8, 'Engineer');

INSERT INTO skill (skill_id, skill_name, skill_level) VALUES (1, 'JAVA', 5);
INSERT INTO skill (skill_id, skill_name, skill_level) VALUES (2, 'JAVASCRIPT', 7);
INSERT INTO skill (skill_id, skill_name, skill_level) VALUES (3, 'PHYTON', 6);

INSERT INTO person_skill(person_id, skill_id) VALUES (1, 2);
INSERT INTO person_skill(person_id, skill_id) VALUES (1, 1);
INSERT INTO person_skill(person_id, skill_id) VALUES (2, 1);
INSERT INTO person_skill(person_id, skill_id) VALUES (2, 3);
INSERT INTO person_skill(person_id, skill_id) VALUES (3, 3);
