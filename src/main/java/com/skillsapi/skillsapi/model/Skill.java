package com.skillsapi.skillsapi.model;

import com.fasterxml.jackson.annotation.JsonBackReference;
import com.fasterxml.jackson.annotation.JsonIdentityInfo;
import com.fasterxml.jackson.annotation.JsonIgnore;
import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.fasterxml.jackson.annotation.ObjectIdGenerators;
import lombok.EqualsAndHashCode;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import lombok.ToString;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.ManyToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.HashSet;
import java.util.Set;

/**
 *  Skill:
 *     type: "object"
 *     required:
 *       - skillName
 *       - skillLevel
 *     properties:
 *       skillName:
 *         type: string
 *       skillLevel:
 *         type: integer
 *     example:
 *       skillName: "REST API's"
 *       skillLevel: 10
 */
@Getter
@Setter
@NoArgsConstructor
@EqualsAndHashCode
@ToString
@Entity
@Table(name = "Skill")
//@JsonIdentityInfo (
//        generator = ObjectIdGenerators.IntSequenceGenerator.class,
//        property = "skill_id"
//)
public class Skill implements Serializable {


    @Id
    @Column(name = "skill_id")
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @NonNull
    private Long skillId;

    @Column(name = "skill_name")
    @NonNull
    private String skillName;

    @Column(name = "skill_level")
    @NonNull
    private Integer skillLevel;

    @ManyToMany(mappedBy = "skills", fetch = FetchType.LAZY)
//    @JsonIgnoreProperties("persons")
//    @JsonBackReference
    @JsonIgnore
    private Set<Person> persons = new HashSet<>();

    public void addPerson(Person person) {
        this.getPersons().add(person);
        person.getSkills().add(this);
    }

    public void removePerson(Person person) {
        this.getPersons().remove(person);
        person.getSkills().remove(this);
    }

//    public Long getSkillId () {
//        return skillId;
//    }
//
//    public String getSkillName () {
//        return skillName;
//    }
//
//    public Integer getSkillLevel () {
//        return skillLevel;
//    }
}
