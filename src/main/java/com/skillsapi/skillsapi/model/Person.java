package com.skillsapi.skillsapi.model;

import com.fasterxml.jackson.annotation.JsonIgnore;
import com.google.common.base.Objects;
import lombok.Getter;
import lombok.NoArgsConstructor;
import lombok.NonNull;
import lombok.Setter;
import org.hibernate.annotations.OnDelete;
import org.hibernate.annotations.OnDeleteAction;

import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.FetchType;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.JoinColumn;
import javax.persistence.JoinTable;
import javax.persistence.ManyToMany;
import javax.persistence.OneToMany;
import javax.persistence.Table;
import java.io.Serializable;
import java.util.Set;

import static com.google.common.base.Objects.*;
import static javax.persistence.CascadeType.ALL;

/**
 * PersonSkills
 * personId:
 *         type: integer
 *         format: int64
 *       fullName:
 *         type: string
 *       yearsOfExperience:
 *         type: integer
 *       educationalQualification:
 *         type: string
 *       skills:
 *         type: array
 *         items:
 *           $ref: '#/definitions/Skill'
 */

@Getter
@Setter
@NoArgsConstructor
@Entity
@Table(name = "Person")
//@JsonIdentityInfo(
//        generator = ObjectIdGenerators.IntSequenceGenerator.class,
//        property = "person_id"
//)
public class Person implements Serializable {

    public Person (String fullName, Integer yearsOfExperience, String educationalQualification) {
        this.fullName = fullName;
        this.yearsOfExperience = yearsOfExperience;
        this.educationalQualification = educationalQualification;
    }

    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Column(name = "person_id")
    private Long personId;

    @Column(name = "full_name")
    @NonNull private String fullName;

    @Column(name = "years_of_experience")
    @NonNull private Integer yearsOfExperience;

    @Column(name = "educational_qualification")
    private String educationalQualification;

    @ManyToMany
//            (cascade = {PERSIST, MERGE}, fetch = FetchType.LAZY)
    @JoinTable(
            name = "Person_Skill",
            joinColumns = {@JoinColumn(name = "person_id", nullable = false) },
            inverseJoinColumns = {@JoinColumn(name = "skill_id", nullable = false)}
    )
    @OnDelete(action = OnDeleteAction.CASCADE)
//    @OrderBy("skill_id ASC")
//    @JsonIgnoreProperties("persons")
//    @Column(nullable = true)
//    @JsonManagedReference
//    @JsonIgnore
    private Set<Skill> skills;

    @OneToMany(cascade = ALL, fetch = FetchType.LAZY)
    @JsonIgnore
//    @JoinColumn(name = "person_id")
    private Set<Category> categories;

    @Override
    public boolean equals (Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        Person person = (Person) o;
        return equal(personId, person.personId) &&
                equal(fullName, person.fullName) &&
                equal(yearsOfExperience, person.yearsOfExperience) &&
                equal(educationalQualification, person.educationalQualification);
    }

    @Override
    public int hashCode () {
        return Objects.hashCode(personId, fullName, yearsOfExperience, educationalQualification);
    }

    @Override
    public String toString () {
        return "PersonSkills{" +
                "personId=" + personId +
                ", fullName='" + fullName + '\'' +
                ", yearsOfExperience=" + yearsOfExperience +
                ", educationalQualification='" + educationalQualification + '\'' +
                '}';
    }
}
