package com.skillsapi.skillsapi.contoller;

import com.skillsapi.skillsapi.model.Category;
import com.skillsapi.skillsapi.model.Person;
import com.skillsapi.skillsapi.model.Skill;
import com.skillsapi.skillsapi.service.IPersonService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Qualifier;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;

@RestController
@RequestMapping("persons")
public class PersonController {

    @Autowired
    @Qualifier("personServiceImpl")
    private IPersonService service;

    @GetMapping
    public List<Person> listSkillsByPerson() {
        return service.listOfPersons();
    }

    @GetMapping("{personId}")
    public Person getPersonSkillsByPersonId(@PathVariable Long personId) {
        return service.listSkillsByPerson(personId);
    }

    @PostMapping
    public Person addPerson(@RequestBody Person person) {
        return service.addPerson(person);
    }

    @PostMapping("skill")
    public Skill addSkill(@RequestBody Skill skill) {
        return service.addSkill(skill);
    }

    @PutMapping("{personId}")
    public Person editPerson(@PathVariable Long personId, @RequestBody Person person) {
        return service.editPerson(personId, person);
    }

    @PutMapping ("{personId}/skill")
    public Person addSkillByPerson(@PathVariable Long personId, @RequestBody Skill skill) {
        return service.addNewSkillByPerson(personId, skill);
    }

    @GetMapping("{personId}/category")
    public List<Category> listCategoriesByPerson(@PathVariable Long personId) {
        return service.categoriesByPerson(personId);
    }

    @PostMapping("{personId}/category")
    public Category addCategory(@PathVariable Long personId, @RequestBody Category category) {
        return service.addNewCategoryByPerson(personId, category);
    }

    @DeleteMapping("{personId}")
    public ResponseEntity<?> deletePerson(@PathVariable Long personId) {

        service.removePerson(personId);
        return ResponseEntity.ok("Person deleted");
    }
}
