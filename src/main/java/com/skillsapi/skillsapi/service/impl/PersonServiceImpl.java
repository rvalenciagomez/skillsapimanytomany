package com.skillsapi.skillsapi.service.impl;

import com.skillsapi.skillsapi.dao.CategoryDao;
import com.skillsapi.skillsapi.dao.PersonDao;
import com.skillsapi.skillsapi.dao.SkillDao;
import com.skillsapi.skillsapi.model.Category;
import com.skillsapi.skillsapi.model.Person;
import com.skillsapi.skillsapi.model.Skill;
import com.skillsapi.skillsapi.service.IPersonService;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import java.util.List;
import java.util.Optional;
import java.util.stream.Collectors;
import java.util.stream.Stream;

@Service("personServiceImpl")
@Slf4j
public class PersonServiceImpl implements IPersonService {

    @Autowired
    private PersonDao personDao;
    @Autowired
    private SkillDao skillDao;
    @Autowired
    private CategoryDao categoryDao;

    @Override
    @Transactional
    public List<Person> listOfPersons () {
        return personDao.findAll();
    }

    @Override
    @Transactional
    public Person listSkillsByPerson(Long personId) {
        return personDao.findByPersonId(personId);
    }

    @Override
    @Transactional
    public Person addPerson(Person person) {
        return personDao.save(person);
    }

    @Override
    @Transactional
    public Skill addSkill(Skill skill) {
        return skillDao.save(skill);
    }

    @Override
    @Transactional
    public Person editPerson (Long personId, Person person) {
        final Person personFound = listSkillsByPerson(personId);
        personFound.setFullName(person.getFullName());
        personFound.setEducationalQualification(person.getEducationalQualification());
        personFound.setYearsOfExperience(person.getYearsOfExperience());
        return addPerson(personFound);
    }

    @Override
    @Transactional
    public Person addNewSkillByPerson (Long personId, Skill skill) {

        log.info("DEBUG: new skill {}", skill.toString());

        return personDao.findById(personId)
                .map(person -> {
                    final Skill skillSaved = skillDao.save(skill);
                    skillSaved.addPerson(person);
                    personDao.save(person);
                    return person;
                }).orElseThrow(()-> new RuntimeException("not able to add skill to person"));
    }

    @Override
    @Transactional
    public List<Category> categoriesByPerson (Long personId) {
        return categoryDao.findByPersonId(personId);
    }

    @Override
    public Category addNewCategoryByPerson (Long personId, Category category) {

        return personDao.findById(personId)
                .map(person -> {
                    category.setPerson(person);
                    return categoryDao.save(category);
                }).orElseThrow(() -> new RuntimeException("not found"));
    }

    @Override
    public void removePerson(Long personId) {
        final Optional<Person> person = personDao.findById(personId);
        if (!person.isPresent()) {
            log.error("Person not found");
            throw new RuntimeException("Person not found");
        }

        try {
            personDao.delete(person.get());
        } catch (Exception e) {
            throw new RuntimeException("Error deleting person", e);
        }
    }


}
