package com.skillsapi.skillsapi.service.stub;

import com.skillsapi.skillsapi.model.Category;
import com.skillsapi.skillsapi.model.Person;
import com.skillsapi.skillsapi.model.Skill;
import com.skillsapi.skillsapi.service.IPersonService;
import org.springframework.stereotype.Service;

import java.util.List;

@Service("personServiceStub")
public class PersonServiceStub implements IPersonService {

    @Override
    public List<Person> listOfPersons () {
        return null;
    }

    @Override
    public Person listSkillsByPerson (Long personId) {

//        final List<Skill> skills = Arrays.asList(new Skill(1L, "Java", 5));
//        return new PersonSkills(1L, "Robert", 8, "Engineer", skills);
        return null;
    }

    @Override
    public Person addPerson (Person person) {
        return null;
    }

    @Override
    public Skill addSkill (Skill skill) {
        return null;
    }

    @Override
    public Person editPerson (Long personId, Person person) {
        return null;
    }

    @Override
    public Person addNewSkillByPerson (Long personId, Skill skill) {
        return null;
    }

    @Override
    public List<Category> categoriesByPerson (Long personId) {
        return null;
    }

    @Override
    public Category addNewCategoryByPerson (Long personId, Category category) {
        return null;
    }

    @Override
    public void removePerson (Long personId) {

    }
}
