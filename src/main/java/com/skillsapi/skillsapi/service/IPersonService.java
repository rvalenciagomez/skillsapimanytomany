package com.skillsapi.skillsapi.service;

import com.skillsapi.skillsapi.model.Category;
import com.skillsapi.skillsapi.model.Person;
import com.skillsapi.skillsapi.model.Skill;

import java.util.List;

public interface IPersonService {

    public List<Person> listOfPersons();

    public Person listSkillsByPerson(Long personId);

    public Person addPerson(Person person);

    public Skill addSkill(Skill skill);

    public Person editPerson(Long personId, Person person);

    public Person addNewSkillByPerson(Long personId, Skill skill);

    public List<Category> categoriesByPerson(Long personId);

    public Category addNewCategoryByPerson(Long personId, Category category);

    public void removePerson(Long personId);
}
