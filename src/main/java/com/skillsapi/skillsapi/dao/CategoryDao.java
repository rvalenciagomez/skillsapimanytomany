package com.skillsapi.skillsapi.dao;

import com.skillsapi.skillsapi.model.Category;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface CategoryDao extends JpaRepository<Category, Long> {

    @Query("FROM Category c JOIN FETCH c.person p WHERE p.personId = :personId")
    public List<Category> findByPersonId(@Param("personId") Long personId);
}
