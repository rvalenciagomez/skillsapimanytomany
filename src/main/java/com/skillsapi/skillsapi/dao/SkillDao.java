package com.skillsapi.skillsapi.dao;

import com.skillsapi.skillsapi.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface SkillDao extends JpaRepository<Skill, Long> {


    @Query("SELECT s FROM Skill s JOIN FETCH s.persons p WHERE p.personId = :personId")
//    @Query(value = "select ps.person_id, ps.skill_id, s.skill_id, s.skill_level, s.skill_name from person_skill ps inner join skill s on ps.skill_id=s.skill_id where ps.person_id=?", nativeQuery = true)
    public List<Skill> getSkillsByPersonId(@Param("personId") Long personId);
}
