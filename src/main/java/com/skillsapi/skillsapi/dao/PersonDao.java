package com.skillsapi.skillsapi.dao;

import com.skillsapi.skillsapi.model.Person;
import com.skillsapi.skillsapi.model.Skill;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

import java.util.List;

public interface PersonDao extends JpaRepository<Person, Long> {

    public Person findByPersonId(Long personId);

    @Query("SELECT p FROM Person p JOIN p.skills s WHERE s.skillId = :skillId")
    public List<Skill> findPersonBySkills(@Param("skillId") Long skillId);
}
