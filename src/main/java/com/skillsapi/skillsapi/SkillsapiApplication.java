package com.skillsapi.skillsapi;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class SkillsapiApplication {

	public static void main(String[] args) {
		SpringApplication.run(SkillsapiApplication.class, args);
	}

}
