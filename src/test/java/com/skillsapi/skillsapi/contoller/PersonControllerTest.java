package com.skillsapi.skillsapi.contoller;

import com.skillsapi.skillsapi.model.Person;
import org.junit.jupiter.api.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;

import java.util.List;

import static org.assertj.core.api.Assertions.assertThat;

@SpringBootTest
class PersonControllerTest {

    @Autowired
    private PersonController controller;

    @Test
    public void testListSkillsByPerson_SuccessPath() {

        final List<Person> personSkills = controller.listSkillsByPerson();
        assertThat(personSkills.size()).isEqualTo(3);
    }

}
